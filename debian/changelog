pydicom (2.4.3-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * Drop transitional package python3-dicom
    Closes: #1032788
  * Standards-Version: 4.6.2 (routine-update)
  * Set upstream metadata fields: Repository.
  * Add missing build-dependency on flit.
  * Build-Depends: texlive-latex-base, texlive-latex-extra, dvipng,
    python3-mpl-sphinx-theme, python3-scipy
  * Hack around strange error of sphinx by simply ignoring it

  [ Nilesh Patra ]
  * Add versioned Builddep against sphinx-gallery to fixup sphinx 7.2 issues
    Closes: #1042612
  * Properly copy tests dir during build
  * Run tests after install step
  * d/clean: Cleanup generated docs

 -- Nilesh Patra <nilesh@debian.org>  Fri, 26 Jan 2024 17:38:13 +0530

pydicom (2.3.1-1) unstable; urgency=medium

  * Team upload
  * Drop dversionmangle from watch file
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 28 Nov 2022 17:58:43 +0100

pydicom (2.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jul 2022 09:56:20 +0200

pydicom (2.2.2-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * Build-Depends: python3-sphinx-copybutton
  * Standards-Version: 4.6.0 (routine-update)
  * autopkgtest-pkg-python.conf: extra_restrictions=needs-internet
  * Build-Depends: python3-gdcm
  * Drop lots of test requiring data files in need to be downloaded
  * Drop gdcm tests due to wrong usage

  [ Steffen Moeller ]
  * Fixed d/watch (github path update)

 -- Andreas Tille <tille@debian.org>  Sat, 11 Dec 2021 10:35:24 +0100

pydicom (2.0.0-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix FTBFS (Closes: #997385)

 -- Nilesh Patra <nilesh@debian.org>  Tue, 02 Nov 2021 17:43:12 +0530

pydicom (2.0.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #964650
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add missing Build-Depends: python3-sphinx-issues

 -- Andreas Tille <tille@debian.org>  Fri, 17 Jul 2020 15:43:36 +0200

pydicom (1.4.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #948877
  * Take over maintenance in Debian Med team
  * Repository layout follows team defaults - so no gbp.conf needed
  * debhelper-compat 10 (routine-update)
  * Standards-Version: 4.5.0
  * d/control: Fix formatting by `cme fix dpkg-control`
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Build-Depends: dh-python
  * Drop Python2 support
    Closes: #937427
  * Fix permissions of *.dcm files

 -- Andreas Tille <tille@debian.org>  Wed, 22 Jan 2020 13:57:10 +0100

pydicom (1.2.1-1) unstable; urgency=medium

  * New upstream release
  * Added debian/gbp.conf to point that we use "debian" branch, not master
    and also we use pristine-tar
  * debian/control
    - Removed gdcm from B-Depends and moved under Suggests since it is an
      optional dependency (Closes: #908555)
  * debian/patches
    - refreshed and upstreamed ones removed/disabled

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 23 Nov 2018 17:32:08 -0500

pydicom (1.1.0-2) unstable; urgency=medium

  * Added transitional python{,3}-dicom packages
  * Fixed path to the built html docs
  * debian/patches
    - 0001-Execute-date-time-tests-only-for-the-backport-to-Pyt.patch
      to skip a test on Python3  to avoid FTBFS with python 3.7
  * debian/rules
    - remove all __pycache__ (could emerge from testing) when
      cleaning/installing
  * debian/control
    - adjusted Vcs fields to point to salsa instead of old anonscm

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 03 Jul 2018 11:01:33 -0400

pydicom (1.1.0-1) neurodebian; urgency=medium

  * New upstream release (Closes: #902389)
    - dicom module was renamed to pydicom, hence binary package rename
    - provides dicom shim Python package to ease migrations
  * debian/control
    - renamed packages to be -pydicom not -dicom.  They now also
      provide/replace and conflict with older python-dicom*
    - adjusted Homepage to point to https://pydicom.github.io
      (Closes: #902388)
    - removed obsolete X-Python fields
    - B-D on python3-sphinx-gallery, python3-sphinx-rtd-theme,
      python3-numpydoc, python{,3}-pytest, python3-matplotlib
      (needs explicit listing in ubuntu backports)
  * debian/copyright
    - updated years and paths
  * debian/
    - adjusted for dicom -> pydicom renames
  * debian/patches
    - all removed (upstreamed etc)
    - deb_skip_missing_handlers as a dirty workaround for skipping when
      optional handlers are not provided
    - deb_xfail_pillow_test - temporary patch to xfail for now
      https://github.com/pydicom/pydicom/issues/663

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 25 Jun 2018 17:47:55 -0400

pydicom (0.9.9-3) unstable; urgency=medium

  * Team upload.
  * Replace python-imaging by python-pil
    Closes: #866458
  * Packae just supports Python3 - thus close bug
    Closes: #782928
  * Secure URI in watch file
  * Standards-Version: 4.1.3
  * Secure Vcs-Git
  * Fix path in copyright

 -- Andreas Tille <tille@debian.org>  Sat, 20 Jan 2018 23:07:33 +0100

pydicom (0.9.9-2) unstable; urgency=medium

  * Update maintainer email.
  * Bump standards version to 3.9.6; no changes necessary.
  * CME-base automatic modernization of debian/control.
  * Switch to debhelper 9.
  * Build python3 package.
  * Build documentation and ship it in a -doc package.

 -- Michael Hanke <mih@debian.org>  Sat, 05 Sep 2015 07:38:24 +0200

pydicom (0.9.9-1) experimental; urgency=medium

  * Fresh upstream release
    - project moved to new website (pydicom.org) and source code hosting
      (Git and github.com), so urls and watch file were adjusted accordingly
  * debian/rules
    - sources are now under source/

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 16 Jan 2015 23:28:46 -0500

pydicom (0.9.8-1) unstable; urgency=medium

  * Fresh upstream release
  * Adjusting X-Python-Version due to
    "pydicom > 0.9.7 requires python 2.6 or later"

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 19 Mar 2014 21:40:46 -0400

pydicom (0.9.7-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches/distribute.patch: Disable downloading of distribute from
    PyPI. (Closes: #733439)

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 11 Feb 2014 11:50:03 +0100

pydicom (0.9.7-1) unstable; urgency=low

  * New upstream release
  * debian/watch: no redirector is needed any longer
  * debian/copyright: DEP5 compliant now
  * debian/control: boost policy to 3.9.3 (no further changes)

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 08 Jun 2012 14:23:30 -0400

pydicom (0.9.6-1) unstable; urgency=low

  * New upstream release
  * Boosted policy compliance to 3.9.2 -- no changes
  * Packaging is no longer done based on a fragile hg2git bridging back to
    the source distribution tarballs:
    - debian/gbp.conf adjusted correspondingly to use 'overlay' approach
    - debian/rules fixed to not cd to sources any longer
  * Converted to use dh_python2
  * Build-depend on python-all and python-numpy for testing

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 18 Nov 2011 15:16:34 -0500

pydicom (0.9.5~rc1-1) experimental; urgency=low

  * New upstream release candidate
  * Boosted policy compliance to 3.9.1 -- no changes are necessary
  * Deprecated debian/patches/up_with_statement_python2.5 -- absorbed upstream
  * Enabled unit-testing in-place

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 13 Sep 2010 22:26:52 -0400

pydicom (0.9.4.1-1) unstable; urgency=low

  * Initial release (Closes: #589098)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 15 Jul 2010 10:03:47 -0400
