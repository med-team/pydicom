Source: pydicom
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Yaroslav Halchenko <debian@onerussian.com>,
           Michael Hanke <mih@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest,
               python3-setuptools,
               python3-setuptools-scm,
               python3-numpy,
               python3-docutils,
               python3-pil,
               python3-sphinx,
               python3-sphinx-gallery (>= 0.10.1-4~),
               python3-sphinx-rtd-theme,
               python3-sphinx-issues,
               python3-sphinx-copybutton,
               python3-mpl-sphinx-theme,
               python3-gdcm,
               python3-matplotlib,
               python3-numpydoc,
               python3-scipy,
               flit,
               texlive-latex-base,
               texlive-latex-extra,
               dvipng,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/pydicom
Vcs-Git: https://salsa.debian.org/med-team/pydicom.git
Homepage: https://pydicom.github.io
Rules-Requires-Root: no

Package: python3-pydicom
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Recommends: python3-numpy,
            python3-pil
Suggests: python3-matplotlib,
          python3-gdcm
Provides: python3-dicom
Description: DICOM medical file reading and writing (Python 3)
 pydicom is a pure Python module for parsing DICOM files.  DICOM is a
 standard (http://medical.nema.org) for communicating medical images
 and related information such as reports and radiotherapy objects.
 .
 pydicom makes it easy to read DICOM files into natural pythonic
 structures for easy manipulation.  Modified datasets can be written
 again to DICOM format files.
 .
 This package installs the module for Python 3.

Package: python-pydicom-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends},
         ${misc:Depends}
Provides: python-dicom-doc
Multi-Arch: foreign
Description: DICOM medical file reading and writing (documentation)
 pydicom is a pure Python module for parsing DICOM files.  DICOM is a
 standard (http://medical.nema.org) for communicating medical images
 and related information such as reports and radiotherapy objects.
 .
 pydicom makes it easy to read DICOM files into natural pythonic
 structures for easy manipulation.  Modified datasets can be written
 again to DICOM format files.
 .
 This package contains the documentation.
